//
//  Practica.swift
//  LocalNotifications
//
//  Created by Salvador Lopez on 27/06/23.
//

import Foundation

//MARK: PRACTICA 2

/**
 Practica: 2
 Objetivo: Reforzar, patron de diseño Factory, y el uso de Notificaciones Locales.
 Requerimientos:
 
 Crear una app, que liste una serie de medicamentos (crear multiples clases que perimitan manejar diferentes tipos de medicamento), y al seleccionar
 cada medicamento pueda ver el detalle de esa prescripcion.
 Cada medicamento tiene la opcion de recordar con una notificacion local la hora del consumo.
 Colocar una clase Factory que retorne diferentes tipos de medicamanto.
 */
