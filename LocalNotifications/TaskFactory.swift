//
//  TaskFactory.swift
//  LocalNotifications
//
//  Created by Salvador Lopez on 27/06/23.
//

import Foundation

class TaskFactory {
    static func createTask(category: TaskCategory, title: String, description: String, date: Date) -> Task {
        switch category {
        case .normal:
            return NormalTask(title: title, description: description, date: date)
        case .urgent:
            return UrgenTask(title: title, description: description, date: date)
        case .recurring:
            return RecurringTask(title: title, description: description, date: date)
        }
    }
}
