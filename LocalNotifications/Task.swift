//
//  Task.swift
//  LocalNotifications
//
//  Created by Salvador Lopez on 27/06/23.
//

import Foundation

// Protocolo que define la interfaz común para las tareas
protocol Task {
    var title: String { get }
    var description: String { get }
    var date: Date { get }
    func execute()
}

// Enum que define las categorias de las tareas
enum TaskCategory {
    case normal
    case urgent
    case recurring
}


