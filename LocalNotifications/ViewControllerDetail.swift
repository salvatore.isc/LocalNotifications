//
//  ViewControllerDetail.swift
//  LocalNotifications
//
//  Created by Salvador Lopez on 27/06/23.
//

import UIKit

class ViewControllerDetail: UIViewController {

    @IBOutlet weak var dateTask: UILabel!
    @IBOutlet weak var descriptionTask: UILabel!
    @IBOutlet weak var titleTask: UILabel!
    @IBOutlet weak var btnRecordar: UIButton!
    
    var titleText = ""
    var descriptionText = ""
    var dateText = ""
    var category: TaskCategory!
    
    @IBAction func launchNotification(_ sender: Any) {
        NotificationManager.scheduleNotification(withTitle: titleText, subtitle: "", body: descriptionText, timeInterval: 10)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        titleTask.text = titleText
        descriptionTask.text = descriptionText
        dateTask.text = dateText
        switch category {
        case .recurring, .urgent:
            btnRecordar.isEnabled = true
        default: btnRecordar.isEnabled = false
        }
    }

}
