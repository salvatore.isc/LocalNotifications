//
//  UrgenTask.swift
//  LocalNotifications
//
//  Created by Salvador Lopez on 27/06/23.
//

import Foundation

class UrgenTask: BaseTask {
    override func execute() {
        print("Ejecutando notificacion para tarea urgente: \(title)")
    }
}
