//
//  RecurringTask.swift
//  LocalNotifications
//
//  Created by Salvador Lopez on 27/06/23.
//

import Foundation

class RecurringTask: BaseTask{
    override func execute() {
        print("Ejecutando notificacion para tarea recurrente: \(title)")
    }
}
