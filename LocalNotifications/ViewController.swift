//
//  ViewController.swift
//  LocalNotifications
//
//  Created by Salvador Lopez on 26/06/23.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UNUserNotificationCenter.current().delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        //NotificationManager.scheduleNotification(withTitle: "Notificación de ejemplo", subtitle: "Este es un subtitulo", body: "Esto corresponde al cuerpo de la notificacion...", timeInterval: 10)
    }

}

//MARK: TABLEVIEW
extension ViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TaskList.tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = TaskList.tasks[indexPath.row].title
        cell.detailTextLabel?.text = TaskList.tasks[indexPath.row].date.formatted()
        if (TaskList.tasks[indexPath.row] as? NormalTask) != nil {
            cell.backgroundColor = .lightGray
        }else if (TaskList.tasks[indexPath.row] as? UrgenTask) != nil {
            cell.backgroundColor = .red
            cell.textLabel?.textColor = .white
            cell.detailTextLabel?.textColor = .white
        }else if (TaskList.tasks[indexPath.row] as? RecurringTask) != nil {
            cell.backgroundColor = .blue
            cell.textLabel?.textColor = .white
            cell.detailTextLabel?.textColor = .white
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(TaskList.tasks[indexPath.row].description)
        if let vcd = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "vcdetail") as? ViewControllerDetail{
            
            //self.present(vcd, animated: true)
            vcd.title = "Detalle"
            vcd.titleText = TaskList.tasks[indexPath.row].title
            vcd.descriptionText = TaskList.tasks[indexPath.row].description
            vcd.dateText = TaskList.tasks[indexPath.row].date.formatted()
            var category:TaskCategory!
            if (TaskList.tasks[indexPath.row] as? NormalTask) != nil {
                category = .normal
            }else if (TaskList.tasks[indexPath.row] as? UrgenTask) != nil {
                category = .urgent
            }else if (TaskList.tasks[indexPath.row] as? RecurringTask) != nil {
                category = .recurring
            }
            vcd.category = category
            navigationController?.pushViewController(vcd, animated: true)
        }
    }
}

//MARK: NOTIFICATIONS
extension ViewController: UNUserNotificationCenterDelegate{
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.banner, .sound, .badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.actionIdentifier == "rememberAction"{
            // reschedule notification
            print("Reschedule notification")
        }else if response.actionIdentifier == "dismissAction"{
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ["TestNotification"])
        }
    }
    
}

