//
//  TaskList.swift
//  LocalNotifications
//
//  Created by Salvador Lopez on 27/06/23.
//

import Foundation

let normalTask = TaskFactory.createTask(category: .normal, title: "Comprar Leche", description: "Comprar 2L de leche deslactosada", date: Date())

let urgetTask  = TaskFactory.createTask(category: .urgent, title: "Estudiar Examen", description: "Estudiar para mi examen de Ingles...", date: Date())

let recurrentTask = TaskFactory.createTask(category: .recurring, title: "Hacer ejercicio", description: "Realizar una caminata de al menos 10 min.", date: Date())

struct TaskList {
    static let tasks = [normalTask,urgetTask,recurrentTask]
}
