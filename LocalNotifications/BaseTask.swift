//
//  BaseTask.swift
//  LocalNotifications
//
//  Created by Salvador Lopez on 27/06/23.
//

import Foundation

class BaseTask: Task {
    
    var title: String
    var description: String
    var date: Date
    
    init(title: String, description: String, date: Date){
        self.title = title
        self.description = description
        self.date = date
    }
    
    func execute() {
        fatalError("Una subclase deberia llamar al metodo execute()")
    }
    
}
