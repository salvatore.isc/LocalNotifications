//
//  NotificationManager.swift
//  LocalNotifications
//
//  Created by Salvador Lopez on 26/06/23.
//

import Foundation
import UserNotifications

class NotificationManager {
    
    static func scheduleNotification(withTitle title:String, subtitle: String, body:String, timeInterval: TimeInterval){
        
        // 1. Trigger
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: false)
        
        // 2. Content
        let content = UNMutableNotificationContent()
        content.title = title
        content.subtitle = subtitle
        content.body = body
        content.sound = .default
        content.badge = 10
        content.categoryIdentifier = "myCategory"
        
        // Añadir acciones y categoria
        let rememberAct = UNNotificationAction(identifier: "rememberAction", title: "Recordar más tarde")
        let dismissAct = UNNotificationAction(identifier: "dismissAction", title: "Descartar")
        let category = UNNotificationCategory(identifier: "myCategory", actions: [rememberAct,dismissAct], intentIdentifiers: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        // Attachments
        
        if let imgPath = Bundle.main.path(forResource: "The_GNU_logo", ofType: "png"){
            let imgURL = URL(fileURLWithPath: imgPath)
            do {
                let attachment = try UNNotificationAttachment(identifier: "The_GNU_logo", url: imgURL)
                content.attachments = [attachment]
            }catch{
                print("Error: \(error)")
            }
        }
        
        // 3. Request
        
        let request = UNNotificationRequest(identifier: "TestNotification", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().add(request){
            error in
            if let error = error {
                print("Se produjo el error: \(error)")
            }
        }
    }
    
}
